///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02d - Mouse `n Cat - EE 205 - Spr 2022
///
/// This C++ program will either:
///   1.  Print the command line arguments in reverse order
///   2.  or, if there are no command line arguments, print all of the
///       environment variables passed into the program
///
/// @file    mouseNcat.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o mouseNcat mouseNcat.cpp
///
/// Usage:  mouseNcat [param1] [param2] ...
///
/// Example:
///   With a command line parameter:
///   $ ./mouseNcat I am Sam
///   Sam
///   am
///   I
///   $
///
///   Without a command line parameter:
///   $ ./mouseNcat
///   SHELL=/bin/bash
///   ...
///   SSH_TTY=/dev/pts/0
///
/// @author  Jaeden Chang <jaedench@hawaii.edu>
/// @date    24_Jan_2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <cstdlib>

// Note the new main() parameter:  envp
int main( int argc, char* argv[], char* envp[] ) {
   if (argc > 1){ // if parameters, print arguments in reverse order
      // for (setup ; condition ; next thing)
         //setup:  argc = length of parameter + 1 due to NULL
            //EX: I am Sam --> argc = 4
         //condition: i>0 means smallest i can be is 1 = last argument to print
         //next thing: increment by -1 bc reverse order
       for (int i = (argc - 1) ; i > 0 ; i--) {
         std::cout << argv[i] << std::endl;
      }
   }

   else { // if no parameters, print all of the environment variables
      for (int j = 0 ; envp[j] ; j++) { 
         std::cout << envp[j] << std::endl; //print environemnt variables 
      }
   }
	//std::cout << "mouseNcat" << std::endl;

   std::exit( EXIT_SUCCESS );
   return 0;
}
